﻿
using System;
using System.Collections.Generic;
using CrystalRpt.Models;
using System.Web;

namespace CrystalRpt.Models
{
    public class Reports
    {
        public Reports()
        {
            ParameterList = new List<Parameter>();
        }
        public List<Parameter> ParameterList { get; set; }
        public string ReportName { get; set; }
    }
}