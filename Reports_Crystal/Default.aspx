﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Reports_Crystal.Default" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .marginLeft {
            margin-left: 15%;
        }
    </style>
</head>
<body style="background-color: beige">
    <asp:Label ID="lblWelcome" runat="server" Text="Welcome to Crystal Report Viewer" Font-Bold="True" Font-Size="Medium"></asp:Label>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <CR:CrystalReportViewer CssClass="marginLeft" ID="CrystalReportViewer1" runat="server" AutoDataBind="true" HasRefreshButton="True" ToolPanelWidth="200px" HasCrystalLogo="False" ToolbarStyle-BackColor="#D8E7A9" ToolbarStyle-BorderColor="#66CCFF" ToolPanelView="None" />
        </div>
    </form>
</body>
</html>
