﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultRedirectErrorPage.aspx.cs" Inherits="Reports_Crystal.Error.DefaultRedirectErrorPage" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>DefaultRedirect Error Page</title>
</head>
<body style="background-color:beige">
    <form id="form1" runat="server">
        <div>
            <h2>Redirect Error Page</h2>
            <br />
            <div>
                <h5 style="padding:0px; margin:2px">Error Discription:</h5>
                <div style="margin-left:100px">
                    <asp:Label ID="errorMsg" runat="server"></asp:Label>
                </div>
            </div>
             <div>
                <h5 style="padding:0px; margin:2px">Inner Exception:</h5>
                <div style="margin-left:100px">
                    <asp:Label ID="innerExpMsg" runat="server"></asp:Label>
                </div>
            </div>
            <br />
            Return to the <a href='Default.aspx'>Default Page</a>
        </div>
    </form>
</body>
</html>
