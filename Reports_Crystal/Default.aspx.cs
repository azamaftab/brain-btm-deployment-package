﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalRpt.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Reports_Crystal
{
    public partial class Default : System.Web.UI.Page
    {
        ReportDocument cryRpt = new ReportDocument();
        string db_Username = ConfigurationManager.AppSettings["db_Username"];
        string db_Password = ConfigurationManager.AppSettings["db_Password"];
        string Server_Name = ConfigurationManager.AppSettings["Server_Name"];
        Reports reports = new Reports();
        string extension = ".rpt";

        protected void Page_Init(object sender, EventArgs e)
        {
            reports.ReportName = Request.Params["rptName"];
            if (reports.ReportName != null)
            {
                lblWelcome.Visible = false;
                string[] urlParameter = Request.Url.Query.Split('&');
                if (urlParameter.Length > 1)
                {
                    CrystalRpt.Models.Parameter parameter;
                    for (int i = 1; i < urlParameter.Length; i++)
                    {
                        parameter = new CrystalRpt.Models.Parameter();
                        string[] param = urlParameter[i].Split('=');
                        if (param.Length == 2)
                        {
                            parameter.ParameterName = param[0];
                            parameter.ParameterValue = param[1];
                            reports.ParameterList.Add(parameter);
                        }
                    }
                }
                LoadReports(reports.ReportName);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //reports.ReportName = Request.Params["rptName"];
            //if (reports.ReportName != null)
            //{
            //    string[] urlParameter = Request.Url.Query.Split('&');
            //    if (urlParameter.Length > 1)
            //    {
            //        CrystalRpt.Models.Parameter parameter;
            //        for (int i = 1; i < urlParameter.Length; i++)
            //        {
            //            parameter = new CrystalRpt.Models.Parameter();
            //            string[] param = urlParameter[i].Split('=');
            //            if (param.Length == 2)
            //            {
            //                parameter.ParameterName = param[0];
            //                parameter.ParameterValue = param[1];
            //                reports.ParameterList.Add(parameter);
            //            }
            //        }
            //    }
            //    LoadReports(reports.ReportName);
            //}

        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            DestroyReportObject();
        }
        private void LoadReports(string reportName)
        {
            //try
            //{
            ConnectionInfo crconnectioninfo = new ConnectionInfo();
            TableLogOnInfos crtablelogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtablelogoninfo = new TableLogOnInfo();
            Tables CrTables;
            crconnectioninfo.ServerName = Server_Name;
            crconnectioninfo.DatabaseName = "";
            crconnectioninfo.UserID = db_Username;
            crconnectioninfo.Password = db_Password;
            var reportPath = Path.Combine(ConfigurationManager.AppSettings["Reports_Directory"], reportName + extension);
            cryRpt.Load(reportPath);
            cryRpt.Refresh();
            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtablelogoninfo = CrTable.LogOnInfo;
                crtablelogoninfo.ConnectionInfo = crconnectioninfo;
                CrTable.ApplyLogOnInfo(crtablelogoninfo);
            }
            if (reports.ParameterList.Count() > 0)
            {
                int ini = 0;
                foreach (var param in reports.ParameterList)
                {
                    cryRpt.SetParameterValue(param.ParameterName.ToString(), param.ParameterValue);
                    // cryRpt.SetParameterValue(ini, param.ParameterValue);
                    //cryRpt.ParameterFields[param.ParameterName].CurrentValues.Add(param.ParameterValue);
                    ini++;
                }
            }
            //cryrpt.RecordSelectionFormula = getCustInfoRptSelection();
           
            CrystalReportViewer1.ReportSource = cryRpt;
           // CrystalReportViewer1.RefreshReport();
            // }
            //catch (Exception ex)
            //{
            //    DestroyReportObject();
            //    throw new Exception(ex.Message);
            //}
        }
        private void DestroyReportObject()
        {
            //try
            //{
            if (cryRpt != null)
            {
                cryRpt.Close();
                cryRpt.Dispose();
            }
            CrystalReportViewer1.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }

    }
}